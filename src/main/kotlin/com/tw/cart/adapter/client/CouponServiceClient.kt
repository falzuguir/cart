package com.tw.cart.adapter.client

import com.tw.cart.boundary.exception.CouponNotFoundException
import com.tw.cart.domain.valueobject.DiscountCoupon
import org.springframework.stereotype.Service

@Service
class CouponServiceClient {

    val couponsList: List<DiscountCoupon> = listOf(
            DiscountCoupon("CART10OFF", 10),
            DiscountCoupon("CART20OFF", 20),
            DiscountCoupon("CART30OFF", 30),
            DiscountCoupon("CART50OFF", 50)
    )

    fun fetchCouponByCode(code: String): DiscountCoupon = couponsList.find { it.couponCode.equals(code, true) }
            .let { it ?: throw CouponNotFoundException("Could not find coupon with code: $code.") }

}