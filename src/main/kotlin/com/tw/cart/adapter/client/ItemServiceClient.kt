package com.tw.cart.adapter.client

import com.tw.cart.boundary.exception.ItemNotFoundException
import com.tw.cart.domain.valueobject.Item
import org.joda.money.BigMoney
import org.joda.money.CurrencyUnit.EUR
import org.springframework.stereotype.Service

@Service
class ItemServiceClient {

    val itemsList: List<Item> = listOf(
            Item(sku = "TW111", displayName = "Soccer Jersey", price = BigMoney.of(EUR, 30.0)),
            Item(sku = "TW222", displayName = "Soccer Ball", price = BigMoney.of(EUR, 40.0)),
            Item(sku = "TW333", displayName = "Winter Gloves", price = BigMoney.of(EUR, 20.0)),
            Item(sku = "TW444", displayName = "Outdoor Jacket", price = BigMoney.of(EUR, 150.0)),
            Item(sku = "TW555", displayName = "Snowboard", price = BigMoney.of(EUR, 100.0))
    )

    fun fetchItemBySku(sku: String): Item = itemsList.find { it.sku.equals(sku, true) }
            .let { it ?: throw ItemNotFoundException("Could not find item with SKU: $sku.") }

}