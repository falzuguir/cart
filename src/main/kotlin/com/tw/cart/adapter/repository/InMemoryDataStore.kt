package com.tw.cart.adapter.repository

import com.tw.cart.boundary.exception.ItemNotInCartException
import com.tw.cart.boundary.exception.ItemQuantityException
import com.tw.cart.domain.domainobject.Cart
import com.tw.cart.domain.event.*
import com.tw.cart.domain.event.EventType.*
import com.tw.cart.domain.valueobject.CartItem
import org.springframework.stereotype.Service

@Service
class InMemoryDataStore {

    val events = mutableListOf<DomainEvent>()
    var cartProjection = Cart(mutableListOf())

    fun storeEvent(event: DomainEvent) =
            events.add(event).also {
                when (event.eventType) {
                    ITEM_QUANTITY_CHANGED -> projectItemQuantityChanged(event = event as ItemQuantityChanged)
                    CART_DELETED -> projectCartDeleted()
                    DISCOUNT_COUPON_ADDED -> projectDiscountCouponAdded(event = event as DiscountCouponAdded)
                    ITEM_ADDED -> projectItemAdded(event = event as ItemAdded)
                    ITEM_REMOVED -> projectItemRemoved(event = event as ItemRemoved)
                }
            }

    private fun projectItemRemoved(event: ItemRemoved) =
            cartProjection.cartItems.removeIf { cartItem -> cartItem.item.sku.equals(event.itemSku, true) }

    private fun projectItemAdded(event: ItemAdded) =
            cartProjection.cartItems.find { cartItem -> cartItem.item.sku.equals(event.cartItem.item.sku, true) }
                    ?.let {
                        cartProjection.cartItems.remove(it)
                        cartProjection.cartItems.add(CartItem(it.item, validateQuantity((it.quantity + event.cartItem.quantity))))
                    }
                    ?: cartProjection.cartItems.add(CartItem(event.cartItem.item, validateQuantity(event.cartItem.quantity)))


    private fun projectItemQuantityChanged(event: ItemQuantityChanged) =
            cartProjection.cartItems.find { cartItem -> cartItem.item.sku.equals(event.itemSku, true) }
                    ?.let {
                        cartProjection.cartItems.remove(it)
                        cartProjection.cartItems.add(CartItem(it.item, validateQuantity(event.newQuantity)))
                    } ?: throw ItemNotInCartException("There is no item with sku: ${event.itemSku} added in the cart.")

    private fun validateQuantity(quantity: Int) =
            if (quantity > 1000 || quantity < 0)
                throw ItemQuantityException("The total quantity of the item in the cart: $quantity is not between 0 and 1000")
            else quantity

    private fun projectCartDeleted() {
        cartProjection = Cart(mutableListOf())
    }

    private fun projectDiscountCouponAdded(event: DiscountCouponAdded) {
        cartProjection = cartProjection.copy(coupon = event.coupon)
    }
}