package com.tw.cart.boundary.dto

data class ItemDTO(
        val sku: String,
        val displayName: String,
        val price: MoneyDTO
)