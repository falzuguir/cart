package com.tw.cart.boundary.dto

data class AddItemRequest(
        val sku: String,
        val quantity: Int
)