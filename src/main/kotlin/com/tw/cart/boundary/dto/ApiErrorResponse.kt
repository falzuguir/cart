package com.tw.cart.boundary.dto

import java.time.Instant

data class ApiErrorResponse(
        val cause: String,
        val timestampInMillis: Long = Instant.now().epochSecond,
        val statusCode: Int,
        val type: String
)