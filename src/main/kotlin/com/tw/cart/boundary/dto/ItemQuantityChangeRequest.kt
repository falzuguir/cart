package com.tw.cart.boundary.dto

data class ItemQuantityChangeRequest(
        val sku: String,
        val newQuantity: Int
)