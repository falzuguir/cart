package com.tw.cart.boundary.dto

import java.math.BigDecimal

data class MoneyDTO(
        val amount: BigDecimal,
        val currency: String
)