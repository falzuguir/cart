package com.tw.cart.boundary.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.tw.cart.domain.valueobject.CartItem
import com.tw.cart.domain.valueobject.DiscountCoupon
import org.joda.money.CurrencyUnit
import java.math.BigDecimal

@JsonInclude(JsonInclude.Include.NON_NULL)
data class CartResponse(
        val cartItems: MutableList<CartItem>,
        val coupon: DiscountCoupon? = null,
        val cartTotal: MoneyDTO = MoneyDTO(BigDecimal.ZERO, CurrencyUnit.EUR.code)
)