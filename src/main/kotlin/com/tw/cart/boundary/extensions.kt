package com.tw.cart.boundary

import com.tw.cart.boundary.dto.ItemDTO
import com.tw.cart.boundary.dto.MoneyDTO
import com.tw.cart.domain.valueobject.Item
import org.joda.money.BigMoney

fun Item.toItemDTO() = ItemDTO(sku = sku, price = price.toMoneyDTO(), displayName = displayName)

fun BigMoney.toMoneyDTO() = MoneyDTO(amount = amount, currency = currencyUnit.code)