package com.tw.cart.boundary.mapper

import com.tw.cart.boundary.dto.CartResponse
import com.tw.cart.boundary.dto.MoneyDTO
import com.tw.cart.domain.domainobject.Cart
import org.joda.money.CurrencyUnit
import java.math.BigDecimal

object CartMapper {

    fun cartToCartResponse(cart: Cart): CartResponse =
            CartResponse(cartItems = cart.cartItems.toMutableList(),
                    cartTotal = MoneyDTO(amount = BigDecimal(cart.calculateTotalWithDiscount()), currency = CurrencyUnit.EUR.code)
            )
}

fun Cart.calculateCarTotal() =
        this.cartItems.map { cartItem -> cartItem.item.price.amount.multiply(BigDecimal(cartItem.quantity)).toInt() }.sum()

fun Cart.calculateTotalWithDiscount() =
        this.coupon?.percentage?.let { percentage ->
            calculateCarTotal().minus(calculateCarTotal().times(percentage).div(100))
        } ?: calculateCarTotal()