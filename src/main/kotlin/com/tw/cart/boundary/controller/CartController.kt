package com.tw.cart.boundary.controller

import com.tw.cart.boundary.dto.AddItemRequest
import com.tw.cart.boundary.dto.CartResponse
import com.tw.cart.boundary.dto.ItemQuantityChangeRequest
import com.tw.cart.domain.service.CartEventHandler
import org.springframework.http.HttpStatus.*
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/carts")
class CartController(val cartEventHandler: CartEventHandler) {

    @GetMapping
    @ResponseStatus(OK)
    fun getCartTotal(): CartResponse = cartEventHandler.generateCartProjection()

    @DeleteMapping
    @ResponseStatus(NO_CONTENT)
    fun deleteCart() = cartEventHandler.handleCartDeleted()

    @DeleteMapping("/items/{sku}")
    @ResponseStatus(OK)
    fun removeItem(@PathVariable sku: String) =
            cartEventHandler.handleItemRemoved(sku).let {
                cartEventHandler.generateCartProjection()
            }

    @PostMapping("/items")
    @ResponseStatus(CREATED)
    fun addItem(@RequestBody addItemRequest: AddItemRequest) =
            cartEventHandler.handleItemAdded(addItemRequest).let {
                cartEventHandler.generateCartProjection()
            }

    @PutMapping("/items")
    @ResponseStatus(OK)
    fun updateQuantity(@RequestBody itemQuantityChangeRequest: ItemQuantityChangeRequest) =
            cartEventHandler.handleItemQuantityChanged(itemQuantityChangeRequest).let {
                cartEventHandler.generateCartProjection()
            }

    @PostMapping("/coupons/{couponCode}")
    @ResponseStatus(CREATED)
    fun applyCoupon(@PathVariable couponCode: String) =
            cartEventHandler.handleDiscountCouponAdded(couponCode).let {
                cartEventHandler.generateCartProjection()
            }

}