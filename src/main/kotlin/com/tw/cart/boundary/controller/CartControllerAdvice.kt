package com.tw.cart.boundary.controller

import com.tw.cart.boundary.dto.ApiErrorResponse
import com.tw.cart.boundary.exception.CouponNotFoundException
import com.tw.cart.boundary.exception.ItemNotFoundException
import com.tw.cart.boundary.exception.ItemNotInCartException
import com.tw.cart.boundary.exception.ItemQuantityException
import mu.KLogging
import org.springframework.http.HttpStatus.*
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice(assignableTypes = [CartController::class])
class CartControllerAdvice {

    @ResponseStatus(NOT_FOUND)
    @ExceptionHandler(value = [CouponNotFoundException::class, ItemNotFoundException::class])
    fun handleNotFound(exception: RuntimeException) =
            ApiErrorResponse(
                    type = NOT_FOUND.name,
                    cause = exception.message ?: "Requested resource was not found.",
                    statusCode = 404
            ).also {
                logger.error(exception) { "User request resulted in NotFound status." }
            }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(value = [ItemNotInCartException::class, ItemQuantityException::class])
    fun handleUserBadRequest(exception: RuntimeException) =
            ApiErrorResponse(
                    type = BAD_REQUEST.name,
                    cause = exception.message ?: "Bad request sent by the client.",
                    statusCode = 400
            ).also {
                logger.error(exception) { "User request resulted in BadRequest status." }
            }

    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = [Exception::class])
    fun handleInternalServerError(exception: Exception) =
            ApiErrorResponse(
                    type = INTERNAL_SERVER_ERROR.name,
                    cause = exception.message ?: "Internal server error.",
                    statusCode = 500
            ).also {
                logger.error(exception) { "User request resulted in InternalServerError status." }
            }

    companion object : KLogging()
}