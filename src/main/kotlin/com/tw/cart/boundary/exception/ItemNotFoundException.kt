package com.tw.cart.boundary.exception

class ItemNotFoundException(override val message: String?) : RuntimeException()