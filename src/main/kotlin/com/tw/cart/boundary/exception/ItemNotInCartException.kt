package com.tw.cart.boundary.exception

class ItemNotInCartException(override val message: String?) : RuntimeException()