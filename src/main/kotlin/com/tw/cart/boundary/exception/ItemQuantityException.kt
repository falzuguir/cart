package com.tw.cart.boundary.exception

class ItemQuantityException(override val message: String?) : RuntimeException()