package com.tw.cart.boundary.exception

class CouponNotFoundException(override val message: String?) : RuntimeException()