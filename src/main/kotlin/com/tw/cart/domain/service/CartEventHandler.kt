package com.tw.cart.domain.service

import com.tw.cart.adapter.client.CouponServiceClient
import com.tw.cart.adapter.client.ItemServiceClient
import com.tw.cart.adapter.repository.InMemoryDataStore
import com.tw.cart.boundary.dto.AddItemRequest
import com.tw.cart.boundary.dto.CartResponse
import com.tw.cart.boundary.dto.ItemQuantityChangeRequest
import com.tw.cart.boundary.mapper.CartMapper
import com.tw.cart.boundary.toItemDTO
import com.tw.cart.domain.event.*
import com.tw.cart.domain.valueobject.CartItem
import org.springframework.stereotype.Service

@Service
class CartEventHandler(
        val inMemoryDataStore: InMemoryDataStore,
        val itemServiceClient: ItemServiceClient,
        val couponServiceClient: CouponServiceClient
) {

    fun generateCartProjection(): CartResponse = CartMapper.cartToCartResponse(inMemoryDataStore.cartProjection.copy())

    fun handleItemAdded(request: AddItemRequest) =
            fetchItemFromClient(request.sku)
                    .let { item ->
                        inMemoryDataStore.storeEvent(ItemAdded(cartItem = CartItem(item = item.toItemDTO(), quantity = request.quantity)))
                    }

    fun handleCartDeleted() = inMemoryDataStore.storeEvent(CartDeleted())

    fun handleDiscountCouponAdded(couponCode: String) =
            couponServiceClient.fetchCouponByCode(couponCode)
                    .let { coupon -> inMemoryDataStore.storeEvent(DiscountCouponAdded(coupon = coupon)) }


    fun handleItemQuantityChanged(request: ItemQuantityChangeRequest) =
            inMemoryDataStore.storeEvent(ItemQuantityChanged(itemSku = request.sku, newQuantity = request.newQuantity))

    fun handleItemRemoved(sku: String) = inMemoryDataStore.storeEvent(ItemRemoved(itemSku = sku))

    private fun fetchItemFromClient(sku: String) = itemServiceClient.fetchItemBySku(sku)
}