package com.tw.cart.domain.event

data class ItemQuantityChanged(
        override val eventType: EventType = TYPE,
        val itemSku: String,
        val newQuantity: Int
) : DomainEvent() {
    companion object {
        val TYPE = EventType.ITEM_QUANTITY_CHANGED
    }
}