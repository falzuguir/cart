package com.tw.cart.domain.event

data class CartDeleted(
        override val eventType: EventType = TYPE
) : DomainEvent() {
    companion object {
        val TYPE = EventType.CART_DELETED
    }
}