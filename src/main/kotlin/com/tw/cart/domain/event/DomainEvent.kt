package com.tw.cart.domain.event

import java.time.Instant

abstract class DomainEvent {
    val dateCreated = Instant.now()
    val version: Int = 0
    abstract val eventType: EventType
}