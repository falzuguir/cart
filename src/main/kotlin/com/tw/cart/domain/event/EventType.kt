package com.tw.cart.domain.event

enum class EventType(val type: String) {
    ITEM_ADDED("ItemAdded"),
    ITEM_REMOVED("ItemRemoved"),
    CART_DELETED("CartDeleted"),
    ITEM_QUANTITY_CHANGED("ItemQuantityChanged"),
    DISCOUNT_COUPON_ADDED("DiscountCouponAdded")
}
