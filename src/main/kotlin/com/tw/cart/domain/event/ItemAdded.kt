package com.tw.cart.domain.event

import com.tw.cart.domain.valueobject.CartItem

data class ItemAdded(
        override val eventType: EventType = TYPE,
        val cartItem: CartItem
) : DomainEvent() {
    companion object {
        val TYPE = EventType.ITEM_ADDED
    }
}