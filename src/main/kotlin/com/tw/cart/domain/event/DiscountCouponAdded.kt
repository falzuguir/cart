package com.tw.cart.domain.event

import com.tw.cart.domain.valueobject.DiscountCoupon

data class DiscountCouponAdded(
        override val eventType: EventType = TYPE,
        val coupon: DiscountCoupon
): DomainEvent()
{
    companion object{
        val TYPE = EventType.DISCOUNT_COUPON_ADDED
    }
}