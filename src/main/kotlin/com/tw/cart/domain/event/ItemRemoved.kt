package com.tw.cart.domain.event

class ItemRemoved(
        override val eventType: EventType = TYPE,
        val itemSku: String
) : DomainEvent() {
    companion object {
        val TYPE = EventType.ITEM_REMOVED
    }
}