package com.tw.cart.domain.domainobject

import com.tw.cart.domain.valueobject.CartItem
import com.tw.cart.domain.valueobject.DiscountCoupon

data class Cart(
        val cartItems: MutableList<CartItem>,
        val coupon: DiscountCoupon? = null
)