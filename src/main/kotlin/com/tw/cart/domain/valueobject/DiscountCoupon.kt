package com.tw.cart.domain.valueobject

data class DiscountCoupon(
        val couponCode: String,
        val percentage: Int
)