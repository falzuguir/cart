package com.tw.cart.domain.valueobject

import com.tw.cart.boundary.dto.ItemDTO

data class CartItem(
        val item: ItemDTO,
        val quantity: Int
)