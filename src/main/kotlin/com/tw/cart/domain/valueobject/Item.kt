package com.tw.cart.domain.valueobject

import org.joda.money.BigMoney

data class Item(
        val sku: String,
        val displayName: String,
        val price: BigMoney
)