package com.tw.cart.adapter.client

import com.tw.cart.boundary.exception.CouponNotFoundException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.springframework.test.context.junit.jupiter.SpringExtension
import kotlin.test.assertFailsWith

@ExtendWith(SpringExtension::class)
class CouponServiceClientTest {

    @InjectMocks
    private lateinit var couponServiceClient: CouponServiceClient

    @Test
    fun `Should fetch Coupon by code successfully`() {
        //GIVEN
        val code = "CART10OFF"

        //WHEN
        val discountCoupon = couponServiceClient.fetchCouponByCode(code)

        //THEN
        assertThat(discountCoupon).isNotNull
    }

    @Test
    fun `Should throw CouponNotFoundException when fetching Coupon by an invalid code`() {
        //GIVEN
        val code = "INVALID"

        //WHEN
        val ex = assertFailsWith<CouponNotFoundException> {
            couponServiceClient.fetchCouponByCode(code)
        }

        //THEN
        assertThat(ex).isInstanceOf(CouponNotFoundException::class.java)
    }
}