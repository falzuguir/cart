package com.tw.cart.adapter.client

import com.tw.cart.boundary.exception.ItemNotFoundException
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.springframework.test.context.junit.jupiter.SpringExtension
import kotlin.test.assertFailsWith

@ExtendWith(SpringExtension::class)
class ItemServiceClientTest {

    @InjectMocks
    private lateinit var itemServiceClient: ItemServiceClient

    @Test
    fun `Should fetch item by sku successfully`() {
        //GIVEN
        val sku = "TW111"

        //WHEN
        val item = itemServiceClient.fetchItemBySku(sku)

        //THEN
        assertThat(item).isNotNull
    }

    @Test
    fun `Should throw ItemNotFoundException when fetching Item by an invalid sku`() {
        //GIVEN
        val sku = "INVALID"

        //WHEN
        val ex = assertFailsWith<ItemNotFoundException> {
            itemServiceClient.fetchItemBySku(sku)
        }

        //THEN
        Assertions.assertThat(ex).isInstanceOf(ItemNotFoundException::class.java)
    }
}