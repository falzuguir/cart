package com.tw.cart.adapter.repository

import com.tw.cart.CartTestFixtures
import com.tw.cart.boundary.exception.ItemNotInCartException
import com.tw.cart.boundary.exception.ItemQuantityException
import com.tw.cart.boundary.toItemDTO
import com.tw.cart.domain.valueobject.CartItem
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.springframework.test.context.junit.jupiter.SpringExtension
import kotlin.test.assertFailsWith

@ExtendWith(SpringExtension::class)
class InMemoryDataStoreTest {

    @InjectMocks
    private lateinit var inMemoryDataStore: InMemoryDataStore

    @Test
    fun `should store and project ItemAdded event`() {
        //GIVEN
        val cartItem = CartItem(CartTestFixtures.givenItems.first().toItemDTO(), 1)
        val event = CartTestFixtures.givenItemAddedEvent(cartItem)

        //WHEN
        inMemoryDataStore.storeEvent(event)

        //THEN
        assertTrue(inMemoryDataStore.cartProjection.cartItems.contains(cartItem))
    }

    @Test
    fun `should throw ItemQuantityException while adding and projecting ItemAdded event with an invalid quantity`() {
        //GIVEN
        val item = CartTestFixtures.givenItems.first()
        val itemAddedEvent = CartTestFixtures.givenItemAddedEvent(CartItem(item.toItemDTO(), 10000))

        //WHEN
        val ex = assertFailsWith<ItemQuantityException> {
            inMemoryDataStore.storeEvent(itemAddedEvent)
        }

        //THEN
        assertThat(ex).isInstanceOf(ItemQuantityException::class.java)
    }

    @Test
    fun `should increase quantity instead of creating a new entry for previously existent product in cart`() {
        //GIVEN
        val cartItem = CartItem(CartTestFixtures.givenItems.first().toItemDTO(), 1)
        val event = CartTestFixtures.givenItemAddedEvent(cartItem)

        //WHEN
        inMemoryDataStore.storeEvent(event)
        inMemoryDataStore.storeEvent(event)

        //THEN
        assertTrue(inMemoryDataStore.cartProjection.cartItems.contains(cartItem.copy(quantity = 2)))
        assertThat(inMemoryDataStore.cartProjection.cartItems.find { item -> item.item.sku.equals(cartItem.item.sku, true) }?.quantity).isEqualTo(2)
    }

    @Test
    fun `should store and project ItemRemoved event`() {
        //GIVEN
        val item = CartTestFixtures.givenItems.first()
        val cartItem = CartItem(item.toItemDTO(), 1)
        val itemAddedEvent = CartTestFixtures.givenItemAddedEvent(cartItem)
        val itemRemovedEvent = CartTestFixtures.givenItemRemovedEvent(item.sku)

        //WHEN
        inMemoryDataStore.storeEvent(itemAddedEvent)
        inMemoryDataStore.storeEvent(itemRemovedEvent)

        //THEN
        assertTrue(inMemoryDataStore.cartProjection.cartItems.isEmpty())
    }

    @Test
    fun `should store and project DiscountCouponAdded event`() {
        //GIVEN
        val event = CartTestFixtures.givenDiscountCouponAddedEvent(CartTestFixtures.givenCoupons.find { it.percentage == 30 }
                ?: CartTestFixtures.givenCoupons.first())

        //WHEN
        inMemoryDataStore.storeEvent(event)

        //THEN
        assertThat(inMemoryDataStore.cartProjection.coupon).isNotNull
    }

    @Test
    fun `should store and project CartDeleted event`() {
        //GIVEN
        val itemAddedEvent = CartTestFixtures.givenItemAddedEvent(CartItem(CartTestFixtures.givenItems.first().toItemDTO(), 1))
        val couponAddedEvent = CartTestFixtures.givenDiscountCouponAddedEvent(CartTestFixtures.givenCoupons.find { it.percentage == 30 }
                ?: CartTestFixtures.givenCoupons.first())
        val cartDeletedEvent = CartTestFixtures.givenCartDeletedEvent

        //WHEN
        inMemoryDataStore.storeEvent(itemAddedEvent)
        inMemoryDataStore.storeEvent(couponAddedEvent)
        inMemoryDataStore.storeEvent(cartDeletedEvent)

        //THEN
        assertThat(inMemoryDataStore.cartProjection.cartItems).isEmpty()
        assertThat(inMemoryDataStore.cartProjection.coupon).isNull()
    }

    @Test
    fun `should store and project ItemQuantityChanged event`() {
        //GIVEN
        val item = CartTestFixtures.givenItems.first()
        val itemAddedEvent = CartTestFixtures.givenItemAddedEvent(CartItem(item.toItemDTO(), 1))
        val itemQuantityChangedEvent = CartTestFixtures.givenItemQuantityChangedEvent(itemSku = item.sku, newQuantity = 5)

        //WHEN
        inMemoryDataStore.storeEvent(itemAddedEvent)
        inMemoryDataStore.storeEvent(itemQuantityChangedEvent)

        //THEN
        assertThat(inMemoryDataStore.cartProjection.cartItems.first().quantity).isEqualTo(5)
    }

    @Test
    fun `should throw ItemNotInCartException while storing and projecting ItemQuantityChanged event with an invalid sku`() {
        //GIVEN
        val item = CartTestFixtures.givenItems.first()
        val itemAddedEvent = CartTestFixtures.givenItemAddedEvent(CartItem(item.toItemDTO(), 1))
        val itemQuantityChangedEvent = CartTestFixtures.givenItemQuantityChangedEvent(itemSku = "INVALID", newQuantity = 5)

        //WHEN
        inMemoryDataStore.storeEvent(itemAddedEvent)
        val ex = assertFailsWith<ItemNotInCartException> {
            inMemoryDataStore.storeEvent(itemQuantityChangedEvent)
        }

        //THEN
        assertThat(ex).isInstanceOf(ItemNotInCartException::class.java)
    }

    @Test
    fun `should throw ItemQuantityException while storing and projecting ItemQuantityChanged event with an invalid quantity`() {
        //GIVEN
        val item = CartTestFixtures.givenItems.first()
        val itemAddedEvent = CartTestFixtures.givenItemAddedEvent(CartItem(item.toItemDTO(), 1))
        val itemQuantityChangedEvent = CartTestFixtures.givenItemQuantityChangedEvent(itemSku = item.sku, newQuantity = 10000)

        //WHEN
        inMemoryDataStore.storeEvent(itemAddedEvent)
        val ex = assertFailsWith<ItemQuantityException> {
            inMemoryDataStore.storeEvent(itemQuantityChangedEvent)
        }

        //THEN
        assertThat(ex).isInstanceOf(ItemQuantityException::class.java)
    }
}