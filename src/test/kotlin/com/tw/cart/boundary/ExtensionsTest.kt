package com.tw.cart.boundary

import com.tw.cart.CartTestFixtures
import org.assertj.core.api.Assertions.assertThat
import org.joda.money.BigMoney
import org.joda.money.CurrencyUnit
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
class ExtensionsTest {

    @Test
    fun `Should convert Item to ItemDTO successfully`() {
        //GIVEN
        val item = CartTestFixtures.givenItems.first()

        //wHEN
        val itemDTO = item.toItemDTO()

        //THEN
        assertThat(item.sku).isEqualToIgnoringCase(itemDTO.sku)
        assertThat(item.price.amount).isEqualTo(itemDTO.price.amount)
        assertThat(item.price.currencyUnit.code).isEqualTo(itemDTO.price.currency)
        assertThat(item.displayName).isEqualTo(itemDTO.displayName)
    }

    @Test
    fun `Should convert BigMoney to MoneyDTO successfully`() {
        //GIVEN
        val bigMoney = BigMoney.of(CurrencyUnit.EUR, 10.0)

        //wHEN
        val moneyDTO = bigMoney.toMoneyDTO()

        //THEN
        assertThat(moneyDTO.amount).isEqualTo(bigMoney.amount)
        assertThat(moneyDTO.currency).isEqualTo(bigMoney.currencyUnit.code)
    }

}