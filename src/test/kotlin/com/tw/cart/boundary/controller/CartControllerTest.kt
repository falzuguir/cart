package com.tw.cart.boundary.controller

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.tw.cart.CartApplication
import com.tw.cart.boundary.dto.AddItemRequest
import com.tw.cart.boundary.dto.ApiErrorResponse
import com.tw.cart.boundary.dto.CartResponse
import com.tw.cart.boundary.dto.ItemQuantityChangeRequest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest(classes = [CartApplication::class])
@AutoConfigureMockMvc
@RunWith(SpringRunner::class)
class CartControllerTest {

    @Autowired
    lateinit var mockMvc: MockMvc

    private val gson: Gson = GsonBuilder().create()

    @Test
    fun `should send request to get Cart total successfully`() {
        //WHEN
        val mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .get("/carts")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
        )

        //THEN
        val response = mvcResult.andExpect(status().isOk).andReturn()
                .let { gson.fromJson(it.response.contentAsString, CartResponse::class.java) }

        assertThat(response).isNotNull
    }

    @Test
    fun `should send request to delete Cart successfully`() {
        //WHEN
        val mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .delete("/carts")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
        )

        //THEN
        mvcResult.andExpect(status().isNoContent)
                .andReturn()
    }

    @Test
    fun `should send request to remove item from Cart successfully`() {
        //GIVEN
        val sku = "TW111"

        //WHEN
        val mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .delete("/carts/items/$sku")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
        )

        //THEN
        val response = mvcResult.andExpect(status().isOk).andReturn()
                .let { gson.fromJson(it.response.contentAsString, CartResponse::class.java) }

        assertThat(response).isNotNull
    }

    @Test
    fun `should send request to add item to Cart successfully`() {
        //GIVEN
        val request = AddItemRequest(sku = "TW111", quantity = 1)

        //WHEN
        val mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/carts/items/")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(request))
        )

        //THEN
        val response = mvcResult.andExpect(status().isCreated).andReturn()
                .let { gson.fromJson(it.response.contentAsString, CartResponse::class.java) }

        assertThat(response).isNotNull
    }

    @Test
    fun `should send request to updateQuantity item to Cart successfully`() {
        //GIVEN
        val addItemRequest = AddItemRequest(sku = "TW111", quantity = 1)
        val changeQuantityRequest = ItemQuantityChangeRequest(sku = "TW111", newQuantity = 3)

        //WHEN
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/carts/items/")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(addItemRequest))
        )

        val mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .put("/carts/items/")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(changeQuantityRequest))
        )

        //THEN
        val response = mvcResult.andExpect(status().isOk).andReturn()
                .let { gson.fromJson(it.response.contentAsString, CartResponse::class.java) }

        assertThat(response.cartItems.first().quantity).isEqualTo(3)
    }

    @Test
    fun `should send request to update item quantity in Cart and fail due to invalid quantity and return APIErrorResponse`() {
        //GIVEN
        val addItemRequest = AddItemRequest(sku = "TW111", quantity = 1)
        val changeQuantityRequest = ItemQuantityChangeRequest(sku = "TW111", newQuantity = 1001)

        //WHEN
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/carts/items/")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(addItemRequest))
        )

        val mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .put("/carts/items/")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(changeQuantityRequest))
        )

        //THEN
        val response = mvcResult.andExpect(status().isBadRequest).andReturn()
                .let { gson.fromJson(it.response.contentAsString, ApiErrorResponse::class.java) }

        assertThat(response.type).isEqualToIgnoringCase(BAD_REQUEST.name)
        assertThat(response.statusCode).isEqualTo(400)
    }

    @Test
    fun `should send request to update item quantity in Cart and fail due to no item found return APIErrorResponse`() {
        //GIVEN
        val addItemRequest = AddItemRequest(sku = "TW111", quantity = 1)
        val changeQuantityRequest = ItemQuantityChangeRequest(sku = "TW112", newQuantity = 100)

        //WHEN
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/carts/items/")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(addItemRequest))
        )

        val mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .put("/carts/items/")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(changeQuantityRequest))
        )

        //THEN
        val response = mvcResult.andExpect(status().isBadRequest).andReturn()
                .let { gson.fromJson(it.response.contentAsString, ApiErrorResponse::class.java) }

        assertThat(response.type).isEqualToIgnoringCase(BAD_REQUEST.name)
        assertThat(response.statusCode).isEqualTo(400)
    }

    @Test
    fun `should send request to apply Coupon to Cart successfully`() {
        //GIVEN
        val coupon = "CART10OFF"

        //WHEN
        val mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/carts/coupons/$coupon")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
        )

        //THEN
        mvcResult.andExpect(status().isCreated).andReturn()
    }

    @Test
    fun `should send request to apply Coupon to Cart and fail due to invalid coupon and return APIErrorResponse`() {
        //GIVEN
        val coupon = "INVALID"

        //WHEN
        val mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/carts/coupons/$coupon")
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
        )

        //THEN
        val response = mvcResult.andExpect(status().isNotFound).andReturn()
                .let { gson.fromJson(it.response.contentAsString, ApiErrorResponse::class.java) }

        assertThat(response.type).isEqualToIgnoringCase(NOT_FOUND.name)
        assertThat(response.statusCode).isEqualTo(404)
    }
}