package com.tw.cart.boundary.mapper

import com.tw.cart.CartTestFixtures
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
class CartMapperTest {

    @Test
    fun `Should map Cart to CartResponse successfully`() {
        //GIVEN
        val cart = CartTestFixtures.givenCart(CartTestFixtures.givenCartItems())

        //WHEN
        val cartResponse = CartMapper.cartToCartResponse(cart)

        //THEN
        assertThat(cart.cartItems).isEqualTo(cartResponse.cartItems)
        assertThat(cart.coupon).isEqualTo(cartResponse.coupon)
    }

    @Test
    fun `Should calculate cart total successfully`() {
        //GIVEN
        val cart = CartTestFixtures.givenCart(CartTestFixtures.givenCartItems())

        //WHEN
        val cartTotalWithoutDiscount = cart.calculateCarTotal()

        //THEN
        assertThat(cartTotalWithoutDiscount).isEqualTo(680)
    }

    @Test
    fun `Should calculate Cart total with Coupon discount`() {
        //GIVEN
        val cart = CartTestFixtures.givenCart(CartTestFixtures.givenCartItems())
        val coupon = CartTestFixtures.givenCoupons.first { it.percentage == 30 }
        val total = cart.calculateCarTotal()

        //WHEN
        val cartTotalWithDiscount = cart.copy(coupon = coupon).calculateTotalWithDiscount()

        //THEN
        assertThat(cartTotalWithDiscount).isEqualTo(total.minus(total.times(coupon.percentage).div(100)))
    }

}