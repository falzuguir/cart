package com.tw.cart

import com.tw.cart.boundary.toItemDTO
import com.tw.cart.domain.domainobject.Cart
import com.tw.cart.domain.event.*
import com.tw.cart.domain.valueobject.CartItem
import com.tw.cart.domain.valueobject.DiscountCoupon
import com.tw.cart.domain.valueobject.Item
import org.joda.money.BigMoney
import org.joda.money.CurrencyUnit.EUR

class CartTestFixtures {
    companion object {

        fun givenCart(list: MutableList<CartItem>) = Cart(list)

        fun givenCartItems() = givenItems.map { CartItem(item = it.toItemDTO(), quantity = 2) }.toMutableList()

        fun givenDiscountCouponAddedEvent(coupon: DiscountCoupon) = DiscountCouponAdded(coupon = coupon)

        fun givenItemAddedEvent(cartItem: CartItem) = ItemAdded(cartItem = cartItem)

        fun givenItemQuantityChangedEvent(itemSku: String, newQuantity: Int) = ItemQuantityChanged(itemSku = itemSku, newQuantity = newQuantity)

        fun givenItemRemovedEvent(itemSku: String) = ItemRemoved(itemSku = itemSku)

        val givenCartDeletedEvent = CartDeleted()

        val givenItems = listOf(
                Item(sku = "TW111", displayName = "Soccer Jersey", price = BigMoney.of(EUR, 30.0)),
                Item(sku = "TW222", displayName = "Soccer Ball", price = BigMoney.of(EUR, 40.0)),
                Item(sku = "TW333", displayName = "Winter Gloves", price = BigMoney.of(EUR, 20.0)),
                Item(sku = "TW444", displayName = "Outdoor Jacket", price = BigMoney.of(EUR, 150.0)),
                Item(sku = "TW555", displayName = "Snowboard", price = BigMoney.of(EUR, 100.0))
        )

        val givenCoupons = listOf(
                DiscountCoupon("CART10OFF", 10),
                DiscountCoupon("CART20OFF", 20),
                DiscountCoupon("CART30OFF", 30),
                DiscountCoupon("CART50OFF", 50)
        )
    }

}