package com.tw.cart.domain.service

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.tw.cart.CartTestFixtures
import com.tw.cart.adapter.client.CouponServiceClient
import com.tw.cart.adapter.client.ItemServiceClient
import com.tw.cart.adapter.repository.InMemoryDataStore
import com.tw.cart.boundary.dto.AddItemRequest
import com.tw.cart.boundary.dto.ItemQuantityChangeRequest
import com.tw.cart.boundary.exception.CouponNotFoundException
import com.tw.cart.boundary.exception.ItemNotFoundException
import com.tw.cart.domain.event.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.springframework.test.context.junit.jupiter.SpringExtension
import kotlin.test.assertFailsWith


@ExtendWith(SpringExtension::class)
class CartEventHandlerTest {

    @Mock
    private lateinit var itemServiceClient: ItemServiceClient
    @Mock
    private lateinit var couponServiceClient: CouponServiceClient
    @Mock
    private lateinit var inMemoryDataStore: InMemoryDataStore

    @InjectMocks
    private lateinit var cartEventHandler: CartEventHandler

    @Test
    fun `should generate Cart projection successfully`() {
        //GIVEN
        val cart = CartTestFixtures.givenCart(CartTestFixtures.givenCartItems())

        whenever(inMemoryDataStore.cartProjection).thenReturn(cart)

        //WHEN
        val result = cartEventHandler.generateCartProjection()

        //THEN
        assertThat(result.cartItems.size).isEqualTo(cart.cartItems.size)
    }

    @Test
    fun `should handle ItemAdded event successfully`() {
        //GIVEN
        val request = AddItemRequest(sku = "TW111", quantity = 2)
        val item = CartTestFixtures.givenItems.first()

        whenever(itemServiceClient.fetchItemBySku(request.sku)).thenReturn(item)
        whenever(inMemoryDataStore.storeEvent(any<ItemAdded>())).thenReturn(true)

        //WHEN
        cartEventHandler.handleItemAdded(request)

        //THEN
        verify(itemServiceClient, times(1)).fetchItemBySku(request.sku)
        verify(inMemoryDataStore, times(1)).storeEvent(any<ItemAdded>())
    }

    @Test
    fun `should throw ItemNotFound exception while handling ItemAdded event`() {
        //GIVEN
        val request = AddItemRequest(sku = "TW111", quantity = 2)

        whenever(itemServiceClient.fetchItemBySku(request.sku)).thenThrow(ItemNotFoundException("Could not find item with SKU: ${request.sku}."))

        //WHEN
        val ex = assertFailsWith<ItemNotFoundException> {
            cartEventHandler.handleItemAdded(request)
        }

        //THEN
        assertThat(ex).isInstanceOf(ItemNotFoundException::class.java)
    }

    @Test
    fun `should handle CartDeleted event successfully`() {
        //GIVEN
        whenever(inMemoryDataStore.storeEvent(any<CartDeleted>())).thenReturn(true)

        //WHEN
        cartEventHandler.handleCartDeleted()

        //THEN
        verify(inMemoryDataStore, times(1)).storeEvent(any<CartDeleted>())
    }

    @Test
    fun `should handle DiscountCouponAdded event successfully`() {
        //GIVEN
        val couponCode = "CART10OFF"
        val coupon = CartTestFixtures.givenCoupons.first()

        whenever(couponServiceClient.fetchCouponByCode(couponCode)).thenReturn(coupon)
        whenever(inMemoryDataStore.storeEvent(DiscountCouponAdded(coupon = coupon))).thenReturn(true)

        //WHEN
        cartEventHandler.handleDiscountCouponAdded(couponCode)

        //THEN
        verify(inMemoryDataStore, times(1)).storeEvent(DiscountCouponAdded(coupon = coupon))
        verify(couponServiceClient, times(1)).fetchCouponByCode(couponCode)
    }

    @Test
    fun `should throw CouponNotFoundException while handling DiscountCouponAdded event`() {
        //GIVEN
        val couponCode = "CART10OFF"

        whenever(couponServiceClient.fetchCouponByCode(couponCode)).thenThrow(CouponNotFoundException("Could not find coupon with code: $couponCode."))

        //WHEN
        val ex = assertFailsWith<CouponNotFoundException> {
            cartEventHandler.handleDiscountCouponAdded(couponCode)
        }

        //THEN
        assertThat(ex).isInstanceOf(CouponNotFoundException::class.java)
    }

    @Test
    fun `should handle ItemQuantityChanged successfully`() {
        //GIVEN
        val request = ItemQuantityChangeRequest(sku = CartTestFixtures.givenItems.first().sku, newQuantity = 2)

        whenever(inMemoryDataStore.storeEvent(any<ItemQuantityChanged>())).thenReturn(true)

        //WHEN
        cartEventHandler.handleItemQuantityChanged(request)

        //THEN
        verify(inMemoryDataStore, times(1)).storeEvent(any<ItemQuantityChanged>())
    }

    @Test
    fun handleItemRemoved() {
        //GIVEN
        val sku = CartTestFixtures.givenItems.first().sku

        whenever(inMemoryDataStore.storeEvent(any<ItemRemoved>())).thenReturn(true)

        //WHEN
        cartEventHandler.handleItemRemoved(sku)

        //THEN
        verify(inMemoryDataStore, times(1)).storeEvent(any<ItemRemoved>())
    }
}